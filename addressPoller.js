const bitcoin = require('bitcoinjs-lib')
const testnet = bitcoin.networks.testnet
const axios = require('axios')
const mongoose = require('mongoose')
const User = require('./models/user')
const Request = require('./models/request')

mongoose.connect('mongodb://localhost:27017/test', function(err) {
  if (!err) {
    console.log('connected to mongo using database test')
  }
})

//Polls for updates to request and account public addresses.  Updates DB collections, logs to console
//
setInterval(function() {

  var user_cursor = User.find().cursor()  // uses streaming instrad of variable buffer
  user_cursor.on('data', function(doc) {
    async function pollPubAdd() {
      var conf_btc = 0
      var unconf_btc = 0
      await axios.all([
          axios.get('https://testnet.blockexplorer.com/api/addr/' + doc.pubaddr + '/utxo'),
          axios.get('https://testnet.blockchain.info/rawaddr/' + doc.pubaddr)
        ])
        .then(axios.spread((conf, unconf) => {
          var txToSpend = []
          conf.data.forEach(function(tx) {
            if (tx.confirmations != 0) {
              txToSpend.push(tx)
              conf_btc = conf_btc + tx.satoshis
            }
          })
          unconf_btc = unconf.data.final_balance
          console.log(conf_btc, ':: accnt balance before conf', doc.pubaddr, doc._id)
          console.log(unconf.data.final_balance, ':: accnt pending balance', doc.pubaddr, doc._id)
        }))
        .catch(err => {
        })
      await User.findOneAndUpdate({ _id: doc._id }, { $set: { "btc": conf_btc, "btc_unconf": unconf_btc } })

    }
    pollPubAdd()
  })
  user_cursor.on('close', function() {
  })

  var req_cursor = Request.find().cursor()
  req_cursor.on('data', function(doc) {
    async function pollReqAdd() {
    var conf_btc = 0
    var unconf_btc = 0
    await axios.all([
        axios.get('https://testnet.blockexplorer.com/api/addr/' + doc.req_pub_key + '/utxo'),
        axios.get('https://testnet.blockchain.info/rawaddr/' + doc.req_pub_key)
      ])
      .then(axios.spread((conf, unconf) => {
        var txToSpend = []
        conf.data.forEach(function(tx) {
          if (tx.confirmations != 0) {
            txToSpend.push(tx)
            conf_btc = conf_btc + tx.satoshis
          }
        })
        unconf_btc = unconf.data.final_balance
        console.log(conf_btc, ':: req balance before conf', doc.req_pub_key, doc.user_name)  // doc of the stream
        console.log(unconf_btc, ':: req pending balance', doc.req_pub_key, doc.user_name)
      }))
      .catch(terror => {
      })
      await Request.findOneAndUpdate({ _id: doc._id }, { $set: { "btc_total": conf_btc, "btc_total_unconf": unconf_btc } })

      }
    pollReqAdd()
  })
  req_cursor.on('close', function() {
  })
}, 5000)

