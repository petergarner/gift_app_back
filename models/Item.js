const mongoose = require('mongoose')

module.exports = mongoose.model('Item',{
    _id: String,
    item: String,
    desc: String,
    pub_key: String,
    value: Number
})
