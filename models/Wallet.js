const mongoose = require('mongoose')

module.exports = mongoose.model('Wallet', {

    _id: String,
    mnemonic: String,
    SeedNode: String

})
