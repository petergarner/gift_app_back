const mongoose = require('mongoose')

module.exports = mongoose.model('User',{

    _id: String,
    pwd: String,
    token: String,
    pubaddr:String,
    req_num: Number,
    user_num: Number,
    btc: Number,
    btc_unconf: Number,

})
