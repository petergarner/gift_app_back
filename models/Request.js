const mongoose = require('mongoose')

module.exports = mongoose.model('Request',{

    _id: Number,
    user_name: String,
    user_req_num: Number,
    req_pub_key: String,
    req_item_name: String,
    req_item_id: String,
    req_value: Number,
    btc_total: Number,
    btc_total_unconf: Number,
    order_complete: Boolean

})
