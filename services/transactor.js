const bitcoin = require('../bitcoinjs-lib')
const testnet = bitcoin.networks.testnet
const pushtx = require('blockchain.info/pushtx').usingNetwork(3)
const coinSelector = require('./coinselector')
const keyMaker = require('./keyMaker')


module.exports = {

  transact: function(dontation_details) {

  this.dontation_details = dontation_details

  // creates a donation transaction from user public address to request address; pushes to blockchain
  const getTrnsData = async() => {
    try {
      let pubAddress = keyMaker.makeKey({ 'master': this.dontation_details.master, buffer: false })
      let pubBuffAddress = keyMaker.makeKey({ 'master': this.dontation_details.master, buffer: true })
      let privateKey = keyMaker.makeKey({ 'master': this.dontation_details.master, buffer: 'private' })

      let payToAddr = this.dontation_details.reqAddr
      let amount = this.dontation_details.amount

      let transactionValues = await coinSelector({ pubAdd: pubAddress, payTo: payToAddr, amount: amount })

      let inputs = transactionValues.inputs
      let outputs = transactionValues.outputs
      let fee = transactionValues.fee

      if (!inputs || !outputs) {
        return { message: 'Not enough bitcoin plus fees for transaction', ststus: false }
      }

      let txb = await new bitcoin.TransactionBuilder(testnet)

      inputs.forEach(input => txb.addInput(input.tx_hash_big_endian, input.tx_output_n))

      outputs.forEach(output => {
        if (!output.address) {
          output.address = pubAddress // change addreess same as user main public addr
        }
        txb.addOutput(output.address, output.value)
      })

      let n = inputs.length
      for (var i = 0; i < n; i++) {
        txb.sign(i, privateKey, pubBuffAddress.redeem.output, null, inputs[i].value)
      }
      let tx = txb.build()
      let txt = tx.toHex()
      console.log(txt, 'Donation made')
      pushtx.pushtx(txt)  // comment out during development
      return { message: 'Donation made. Thank you!', status: true }
    } catch (err) {
      console.log(err)
        return { message: 'Problem with donation', status: false }
    }
  }
  return getTrnsData()
}
}
