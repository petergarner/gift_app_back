const axios = require('axios')
const coinSelect = require('../coinselect')

module.exports = async function(selctorObj) {

  const pubAddress = selctorObj.pubAdd
  const payToAddr = selctorObj.payTo
  const amount = selctorObj.amount
  var txData = {}
  // retrieves list of unspent outputs and fee rate from public APIs, calls Conselect library
  try {
    await axios.all([
        axios.get('https://testnet.blockchain.info/unspent?active=' + pubAddress),
        axios.get('https://bitcoinfees.earn.com/api/v1/fees/recommended')
      ])
      .then(axios.spread((resp_txid, resp_fee) => {
        var txToSpend = []
        resp_txid.data.unspent_outputs.forEach((tx) => {
          txToSpend.push(tx)
        })
        return [txToSpend, resp_fee.data.fastestFee]

      }))
      .then((fee_tx_arr) => {
        let utxos = fee_tx_arr[0]
        let feeRate = fee_tx_arr[1]
        let targets = [{
          address: payToAddr,
          value: amount
        }]
        let { inputs, outputs, fee } = coinSelect(utxos, targets, feeRate)
        txData = { inputs: inputs, outputs: outputs, fee: fee }
      })
    return txData
  } catch (err) {
    console.log(err)
    return "Problem contacting APIs or with API data"
  }
}
