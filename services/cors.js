module.exports = function (req, res, next) {
  // controls header type acceptance for express http server
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    next()

}
