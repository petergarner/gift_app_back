const jwt = require('jwt-simple')
const moment = require('moment')
const url = require('url')

module.exports = {

  // decondes / confirms token from url if present; checks expiry
  getDecode: function(req, res, next) {

    let parts = url.parse(req.url, true)
    let tokenKeyVal = parts.query
    let tokenKeys = Object.keys(tokenKeyVal)

    if (tokenKeys.includes('token')) {
      let token = tokenKeyVal.token

      let payload = jwt.decode(token, 'secret') // secret updated regularly
      if (payload.exp <= moment().unix()) {
        res.statusMessage = 'Token has expired'
        res.send()
        return

      } else {
        req.user = payload.sub // extracts user from decrypted token
        req.friend = tokenKeyVal.friend
        next()
      }
    } else {

      res.statusMessage = 'No token present'
      res.send()
      return
    }

  },

  // decodes / confirms token withing http headers; checks header fields and token expiry
  postDecode: function(req, res, next) {

    if (!req.header('Authorization')) {
      res.status(401).send({ message: 'Incorrect Authorization header' })
      return
    }
    let token = req.header('Authorization').split(' ')[1]

    let payload = jwt.decode(token, 'secret') // secret updated regularly
    if (payload.exp <= moment().unix()) {
      res.statusMessage = 'Token has expired'
      res.send()
      return
    }
    req.user = payload.sub // extracts user from decrypted token
    next()
  }
}
