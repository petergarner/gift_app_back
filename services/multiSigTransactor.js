const bitcoin = require('bitcoinjs-lib')
const testnet = bitcoin.networks.testnet
const pushtx = require('blockchain.info/pushtx').usingNetwork(3)
const coinSelector = require('./coinselector')
const keyMaker = require('./keyMaker')
const bip32 = require('bip32')



module.exports = {

  makeMultiTransaction: function(buy_details) {

    this.buy_details = buy_details
    this.BAD_RESULT = { message:'Problem with transaction. Purchace not made', status: false }
    this.TEMP_FEE_REDUCE = 4000  // used in process of pushing fees onto merchant

    // creates a multisignature transaction and pushes to blockchain
    const runMultiTrans = async() => {

      try {
        let payToAddr = this.buy_details.pub_key
        let amount_original = this.buy_details.amount
        let amount_temp = this.buy_details.amount - this.TEMP_FEE_REDUCE // this will still consume all the inputs (minimum 70000) but allow for cs to return a transaction                                        // fee that includes all inputs and transaction data
        let pubAddress = this.buy_details.req_pub_key

        let transactionValues = await coinSelector({ pubAdd: pubAddress, payTo: payToAddr, amount: amount_temp })

        let inputs = transactionValues.inputs
        let outputs = transactionValues.outputs
        let fee = transactionValues.fee

        if (!inputs || !outputs) {
          return this.BAD_RESULT
        }

        let amount_less_fee = amount_temp + this.TEMP_FEE_REDUCE - fee // invert the fees (deduct inststead of add)

        const txb = new bitcoin.TransactionBuilder(testnet)

        // retrieves all signing requirments
        let p2sh = keyMaker.makeMultiKey(this.buy_details, 'p2sh')
        let p2wsh = keyMaker.makeMultiKey(this.buy_details, 'p2wsh')
        let privateKeys = keyMaker.makeMultiKey(this.buy_details, 'private')

        let firstPrivkey = privateKeys[0]
        let secondPrivkey = privateKeys[1]

        inputs.forEach(input => txb.addInput(input.tx_hash_big_endian, input.tx_output_n, null, p2sh.output))

        txb.addOutput(payToAddr, amount_less_fee) // no change address (given to miners as above amount_less_fee)

        let n = inputs.length
        for (var i = 0; i < n; i++) {
          txb.sign(i, firstPrivkey, p2sh.redeem.output, null, inputs[i].value, p2wsh.redeem.output)
          txb.sign(i, secondPrivkey, p2sh.redeem.output, null, inputs[i].value, p2wsh.redeem.output)
        }
        let tx = txb.build()
        let txt = tx.toHex()
        console.log(txt,'Item ordered')
        pushtx.pushtx(txt)  // comment out during development
        return { message: "Item ordered!", status: true }
      } catch (err) {
        console.log(err)
        return  this.BAD_RESULT
      }
    }
    return runMultiTrans()
  }
}
