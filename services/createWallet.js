const bip39 = require('bip39')
const bitcoin = require('../bitcoinjs-lib')
const testnet = bitcoin.networks.testnet
const bip32 = require('bip32')
const User = require('../models/user')
const Wallet = require('../models/wallet')
const keyMaker = require('./keymaker')

module.exports = function(name) {

  // steps through wallet generation proces using mnemonic generator as entropy for seed
  // creates and public bitcoin address and adds to new user collection
  const createWallet = async() => {
    try {
      let mnemonic = bip39.generateMnemonic()
      let seed = bip39.mnemonicToSeed(mnemonic)
      let master = bip32.fromSeed(seed, testnet)
      let master_serilazed = master.toBase58(); // serialised for storage

      let wallet_data = {
        _id: name,
        mnemonic: mnemonic,
        SeedNode: master_serilazed
      }

      var wallet = new Wallet(wallet_data)

      await wallet.save()

      let pubAddress = keyMaker.makeKey({ 'master': master_serilazed, 'buffer': false })

      await User.findOneAndUpdate({ _id: name }, { pubaddr: pubAddress }, { upsert: true })

      return `New wallet and public key created for ${name}`

    } catch (err) {
      console.log(err)
      res.statusMessage = "problem creating wallet"
      res.send()
      return
    }
  }

  return createWallet()

}
