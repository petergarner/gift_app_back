const bitcoin = require('../bitcoinjs-lib')
const testnet = bitcoin.networks.testnet
const bip32 = require('bip32')
const Wallet = require('../models/wallet')
const Request = require('../models/request')
const User = require('../models/user')

module.exports = {
  // takes a serilased master private key and return type
  // returns either a public address, a private key for signing transactions
  // and a p2sh(SegWit) object for redeeming funds from user public addresses
  makeKey: function(keyAndType) {

    function getAddressOrKey(derived, buffer) {

      if (buffer) {
        let p2sh = bitcoin.payments.p2sh({
          redeem: bitcoin.payments.p2wpkh({ pubkey: derived.publicKey, network: bitcoin.networks.testnet }),
          network: bitcoin.networks.testnet
        })
        return p2sh // pubkey buffer (for redeem.output)
      }
      let { address } = bitcoin.payments.p2sh({
        redeem: bitcoin.payments.p2wpkh({ pubkey: derived.publicKey, network: bitcoin.networks.testnet }),
        network: bitcoin.networks.testnet
      })
      return address // public address
    }

    let master = bip32.fromBase58(keyAndType.master, testnet)
    let derived = master.derivePath("m/44'/0'/0'/0/0")
    if (keyAndType.buffer === 'private') {
      return derived
    }

    return getAddressOrKey(derived, keyAndType.buffer)
  },

  // takes a object containing app and user private keys with user and user request numbers, and return type
  // returns either a public multisig address, an array of private key sfor signing transactions
  // and a p2sh(p2wsh) object for redeeming along with a p2wsh object, also for redeeming
  makeMultiKey: function(reqObj, keyType) {

    let userRootNum = reqObj.user_num
    let user_reqNum = reqObj.user_req_num
    let user_key = reqObj.user_key
    let app_key = reqObj.app_key

    // creates and recreates derivation at depth 4 and 5
    let app_master = bip32.fromBase58(app_key, testnet)
    let app_derived = app_master.derivePath("m/44'/0'/0'/" + userRootNum + '/' + user_reqNum)
    // created and recreates derivation at depth 5
    let user_master = bip32.fromBase58(user_key, testnet)
    let user_derived = user_master.derivePath("m/44'/0'/0'/0/" + user_reqNum)
    console.log(user_derived.publicKey)

    let privateKeys = [app_derived, user_derived]

    if (keyType == 'private') {
      return privateKeys
    }

    function getMutiAddressOrKey(app_derived, user_derived, keytype) {

      let pubkeys = [app_derived.publicKey, user_derived.publicKey]
      let p2ms = bitcoin.payments.p2ms({ m: 2, pubkeys, network: testnet }) // multisig object, 2:2 quorum
      let p2wsh = bitcoin.payments.p2wsh({ redeem: p2ms, network: testnet })


      if (keytype == 'p2wsh') {
        return p2wsh
      }
      let p2sh = bitcoin.payments.p2sh({ redeem: p2wsh, network: testnet })

      if (keytype == 'p2sh') {
        return p2sh
      }
      let { address } = bitcoin.payments.p2sh({ redeem: p2wsh, network: testnet })

      if (keytype == 'address') {
        return address
      }
    }
    return getMutiAddressOrKey(app_derived, user_derived, keyType)
  }


}

