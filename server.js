const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const auth = require('./controllers/auth')
const request = require('./controllers/request')
const donate = require('./controllers/donate')
const buyItem = require('./controllers/buyitem')
const account = require('./controllers/account')
const items = require('./controllers/items')
const checkToken = require('./services/checktoken')
const cors = require('./services/cors')


app.use(bodyParser.json())
app.use(cors)

app.post('/auth/register', auth.register)
app.post('/auth/login', auth.login)

app.get('/api/accountwallet', checkToken.getDecode, account.getPubAddr)
app.get('/api/accountcontacts', checkToken.getDecode, account.getContacts)

app.post('/api/requests', checkToken.postDecode, request.post)
app.get('/api/requests', checkToken.getDecode, request.get)

app.post('/api/donate', checkToken.postDecode, donate.post)

app.get('/api/items', checkToken.getDecode, items.get)

app.post('/api/buyitem', checkToken.postDecode, buyItem.buy)

//DBConnection
mongoose.connect('mongodb://localhost:27017/giftcoin', { useNewUrlParser: true }, function(err) { // switch to giftcoin_db
    if (!err) {
        console.log('connected to mongo using database giftcoin')
    }
})

var server = app.listen(5000, function() {
    console.log('listening on port ', server.address().port)
})

