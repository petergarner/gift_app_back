const chai = require('chai')
const should = chai.should()

const keyMaker = require('../../../services/keyMaker')
var mongoose = require('mongoose')

describe('keyMaker processes', function() {
  context('Public account address generation', function() {
    it('should return a public key from a valid keytype object', function() {
      const master_serilazed = 'tprv8ZgxMBicQKsPd25cnsYKjBc6y6QwuKkqVQ6A6Un3dzqkatUiCEK3MTGXqkUpDMSRKurSJ3LApCbXnLzAhXE9P4iCaxUvFyVGFaf6ZzZQtBe'
      const keytype_obj = { 'master': master_serilazed, 'buffer': false }
      keyMaker.makeKey(keytype_obj)
        .should.equal('2N5WJ54zAnbUtU1wS33zPVyuFgP8B31qY2N')
    })

  })

})

describe('keyMaker processes', function() {
  context('multikey request address generation', function() {
    it('should create a "request public key from a passed request object (with correct derivation)', function() {
      const reqObj = {
        user_name: 'test@test',
        req_item_name: 'MacBook',
        user_seed: 'tprv8ZgxMBicQKsPd6Pytv2tY4XaWrFyX3yECsCSFC4SvZZAj9RsUkXJkijT7wFSmrFZHByNr5gUTaaHZ2o91e7xu6dLHvxXfHCTyuUkcpwLzK9',
        user_req_num: 1,
        user_num: 1,
        app_seed: 'tprv8ZgxMBicQKsPetP466FWr6WoFtCz87Na5m4BJMQ7CQ6J7g9dAZgnVsVRKUpzowzD17cBqceDsLFBBSQr55Pp3fnm1nRkMr5qwe2oMHW1A2V'
      }
      keyMaker.makeMultiKey(reqObj, 'address')
        .should.equal('2N7WcxG1rRyxss5g9gpcG7vq6ruw6pTcnBB')  // having redefined keys confims derivation
    })
  })
})




