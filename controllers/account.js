const User = require('../models/user')

module.exports = {
  // returns a full list of application users excluding app@app and test@test
  getContacts: function(req, res) {

    let user = req.user
    try {
      User.find({}).exec().then((col) => { // returns the whole collection
        var usrSend = []
        var usr
        for (usr = 0; usr < col.length; usr++) {
          if (col[usr]._id != user && col[usr]._id != 'app@app' && col[usr]._id != 'test@test') {
            usrSend.push([col[usr]._id])
          }
        }
        res.send(usrSend)
      })
    } catch (err) {
      console.log(err)
      res.statusMessage = "Problem loading contacts"
      res.send()
      return
    }
  },

// returns the public adderess and other account details of the requesitng user
  getPubAddr: function(req, res) {
    var account = []

    try {
      User.find({ _id: req.user }, function(error, doc) {
        account = {
          name: doc[0]._id,
          pubaddr: doc[0].pubaddr,
          btc_unconf: doc[0].btc_unconf
        }
        res.send(account)
      })
    } catch (err) {
      console.log(err)
      res.statusMessage = "Problem loadig public key"
      res.send()
      return
    }

  }
}
