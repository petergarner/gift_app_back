const User = require('../models/user')
const Wallet = require('../models/wallet')
const jwt = require('jwt-simple')
const moment = require('moment')
const CreateWallet = require('../services/createwallet')

module.exports = {

 // sets up user account and initialises values; returns a jwt to the browser for auth purposes
  register: function(req, res) {

    const runRegister = async() => {
      try {
        var user = {}
        user._id = req.body.email
        user.pwd = req.body.pwd
        user.btc = 0,
        user.btc_unconf = 0

        user.req_num = 0; // no requests so far

        await User.findOne({ _id: user._id })
          .exec().then((userExists) => {
            if (userExists) {
              throw 'Email is already registered'
            }
          })

        await User.countDocuments({})
          .exec().then((usr_count) => { user.user_num = usr_count })

        var newUser = new User(user)
        await newUser.save()

        let token = createToken({ _id: user._id, pwd: user.pwd })
        res.json({ token: token })

        CreateWallet(user._id);
        console.log('new wallet created')

      } catch (err) {
        console.log(err)
        res.statusMessage = err
        res.send()
        return
      }
    }
    return runRegister()
  },

  // services login API, checks password, returns jwt or errors
  login: function(req, res) {

    this.LOGIN_ERROR = 'Invalid email or password'

    this.loginUser = req.body.email
    this.pwd = req.body.pwd

    const runLogin = async() => {
      try {
        await User.findOne({ _id: this.loginUser })
          .exec().then((user) => {
            if (!user) {
              throw this.LOGIN_ERROR
            }
            if (this.pwd === user.pwd) {
              let token = createToken({ _id: this.loginUser, pwd: pwd })
              res.json({ token: token })
            } else {
              throw this.LOGIN_ERROR
            }
          })
      } catch (err) {
        console.log(err)
        res.statusMessage = this.LOGIN_ERROR
        res.send()
        return
      }
    }
    return runLogin()
  }
}

// creates JSON Web Token (jwt) for token based authentication
function createToken(user) {
  try {
    var payload = {
      sub: user._id,
      iat: moment().unix(),
      exp: moment().add(14, 'days').unix()
    };
    return jwt.encode(payload, 'secret')
  } catch (err) {
    console.log(err)
    res.statusMessage = "Authentication problem"
    res.send()
    return
  }
}
