const Request = require('../models/request')
const Item = require('../models/item')
const Wallet = require('../models/wallet')
const User = require('../models/user')
const multiSigTransactor = require('../services/multisigtransactor')

module.exports = {
  // responds buy order and triggers multisignature transaction
  buy: function(req, res) {

    this.user = req.body.name
    this.item_id = req.body.item
    this.APP_WALLET = 'app@app'
    this.request_id;

    const runBuy = async() => {

      requestQuery = Request.findOne({ $and: [{ user_name: { '$eq': this.user } }, { req_item_id: { '$eq': this.item_id } }] })

      buyObj = {}
      try {
        await requestQuery.exec().then((data) => {
            if (data.order_complete) {
              errorMessage('Order already placed')
            }
            if (data.btc_total_unconf !== data.req_value ) {
              errorMessage()
            }

            buyObj.amount = data.req_value
            buyObj.req_pub_key = data.req_pub_key
            buyObj.user_req_num = data.user_req_num
          })
          .catch(err => {
            errorMessage()
          })

        await Wallet.findOne({ _id: this.APP_WALLET }).exec().then((appdoc) => {
            buyObj.app_key = appdoc.SeedNode
          })
          .catch(err => {
            errorMessage()
          })

        await Wallet.findOne({ _id: this.user }).exec().then((usrdoc) => {
            buyObj.user_key = usrdoc.SeedNode
          })
          .catch(err => {
            errorMessage()
          })

        await User.findOne({ _id: this.user })
          .exec().then((user) => {
            buyObj.user_num = user.user_num
          })
          .catch(err => {
            errorMessage()
          })

        await Item.findOne({ _id: this.item_id })
          .exec().then((item) => {
            buyObj.pub_key = item.pub_key
          })
          .catch(err => {
            errorMessage()
          })

        var transaction = await multiSigTransactor.makeMultiTransaction(buyObj)

        if (transaction.status) {

          await requestQuery.exec().then((data) => {
            data.order_complete = true
            data.save()
          })

          res.statusMessage = transaction.message
          res.send()
          return
        }
        res.statusMessage = transaction.message
        res.send()

        function errorMessage(msg) {
          if (!msg) {
            let msg = 'Probem with order. Order NOT made'
          }
            res.statusMessage = `${ msg }`
            res.send()
            throw msg
        }
      } catch (err) {
        console.log(err)
        return
      }
    }
    runBuy()
  }
}
