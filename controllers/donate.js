const Wallet = require('../models/wallet')
const Request = require('../models/request')
const User = require('../models/user')
const transactor = require('../services/transactor')


module.exports = {
  // responds to donation providing checks; triggers transaction if accepted; returns result message from transactor
  post: function(req, res) {

    this.amount = req.body.donation
    this.item_id = req.body.item_id
    this.friend = req.body.friend
    this.user = req.user
    this.min_donation = 70000 // minimum dontaion allowd
    this.requestDoc = {}
    this.userFundsAvailable = 0

    const runDonate = async() => {

      // Some procedures replicates front end from end checks

      if (this.amount == null || this.amount == 'undefined' || this.amount < this.min_donation) {
        return
      }

      let userQuery = User.findOne({ _id: this.user })
      let walletQuery = Wallet.findOne({ _id: this.user })
      let requestQuery = Request.findOne({ $and: [{ user_name: { '$eq': this.friend } }, { req_item_id: { '$eq': this.item_id } }] })

      try {
        await requestQuery.exec().then((doc) => {
          if (!doc) {
            errorMessage(true, 'problem finding request')
          }
          this.requestDoc = doc
        })

        this.userFundsAvailable = await userQuery.exec().then(udoc => { return udoc.btc_unconf })
          .catch(err => {
            errorMessage(true)
          })

        if (this.requestDoc.req_value === this.requestDoc.btc_total_unconf) {
          errorMessage(true, 'request fullfilled.  No more donations required')
        }

        let amount_required = this.requestDoc.req_value - this.requestDoc.btc_total_unconf

        if (this.userFundsAvailable < this.amount) {
          errorMessage(true, `Not enough funds plus fees. You only have ${ this.userFundsAvailable } Satoshis`)
        }

        if (this.amount > amount_required) {
          errorMessage(true, `Donation to high. ${ this.amount } is more than the remaining value of ${ amount_required }`)
        }
        if (amount_required < this.min_donation) {
          this.min_donation = amount_required
        }
        if (this.amount < this.min_donation) {
          errorMessage(true, `${ amount_required } Satoshis is the minimum donation!!!`)
        }

        function errorMessage(err, msg) {
          if (!msg) {
            msg = 'Probem with request'
          }
          if (err) {
            res.statusMessage = `${msg}!`
            res.send()
            throw msg
          }
        }
      } catch (err) {
        console.log(err)
        return
      }
      // takes the private key for the donator and the existing request public key, passes to transactor
      try {
        var tansactionStatus;
        await Promise.all([
            walletQuery.exec().then((wdoc) => wdoc.SeedNode), // get the master serialized key for the donator
            requestQuery.exec().then((rdoc) => rdoc.req_pub_key) // get the key for the specific request
          ])
          .then((dbVals) => {
            return dbVals[0].length > dbVals[1].length ? // the master serialised key is more than twice as long (also longer than bech32s)
              transactor.transact({ master: dbVals[0], reqAddr: dbVals[1], amount: this.amount }) :
              transactor.transact({ master: dbVals[1], reqAddr: dbVals[0], amount: this.amount })
          })
          .then((transaction) => {
            if (transaction.status) {
              res.statusMessage = transaction.message
              res.send()
              return
            }
            res.statusMessage = transaction.message
            res.send()
          })
      } catch (err) {
        res.statusMessage = 'Problem with transaction; Not sent.'
        res.send() // error passed up from lower calls
        return
      }
    }
    runDonate()

  }
}
