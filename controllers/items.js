const Item = require('../models/item');

module.exports = {

  // resposible for returning item details for shopping page
  get: function(req, res) {

    const retreiveItems = async() => {
      try {
        Item.find({}).exec().then((col) => { // returns the whole collection
          var itemSend = {}
          var itm
          for (itm = 0; itm < col.length; itm++) {
            let item = col[itm].item
            let id = col[itm]._id
            let value = col[itm].value
            if (item != null) {
              itemSend[id] = value
            }
          }
          res.send(itemSend)
        })
      } catch (err) {
        console.log(err)
        res.statusMessage = "Problem getting items"
        res.send()
      }
    }
    return retreiveItems()
  }
}
