const Item = require('../models/item')
const User = require('../models/user')
const Request = require('../models/request')
const keyMaker = require('../services/keymaker')
const Wallet = require('../models/wallet')

module.exports = {

  post: function(req, res) {

    this.user = req.user
    this.item_id = req.body.item
    this.APP_WALLET = 'app@app'
    this.error = false

    if (!this.item_id) {
      return
    }
    // creates request object, called keymaker for multisig address; saves to new collection
    const generateRequest = async() => {
      try {
        await Item.find({}) // returns all in collection
          .exec()
          .then((items) => {
            var itemList = []
            items.forEach(item => itemList.push(item._id))
            if (!itemList.includes(this.item_id)) {
              errorMessage("Item not available, check item code")
            }
          })
          .catch(err => {
            errorMessage()
          })

        await Request.findOne({ $and: [{ user_name: { '$eq': this.user } }, { req_item_id: { '$eq': this.item_id } }] })
          .exec()
          .then((data) => {
            if (data) {
              errorMessage("Item already requested")
            }
          })

        var reqObj = {}

        await Item.findOne({ _id: this.item_id }).exec().then((item) => {
            reqObj.req_value = item.value
            reqObj.user_name = this.user
            reqObj.req_item_name = item.item
            reqObj.req_item_id = this.item_id
            reqObj.btc_total = 0 //  inital BTC
            reqObj.btc_total_unconf = 0 //
            reqObj.order_complete = false

          })
          .catch(err => {
            errorMessage()
          })

        await Request.countDocuments({}).exec().then((req_count) => {
            reqObj._id = req_count + 1
          })
          .catch(err => {
            errorMessage()
          })

        await Wallet.findOne({ _id: this.user }).exec().then((doc) => {
            reqObj.user_key = doc.SeedNode
          })
          .catch(err => {
            errorMessage()
          })

        await User.findOneAndUpdate({ _id: this.user }, { $inc: { req_num: 1 } })
          .catch(err => { // rolls back increment
            User.findOneAndUpdate({ _id: this.user }, { $set: { req_num: reqObj._id - 1 } }).exec()
            then(() => {
              errorMessage()
            })
          })

        await User.findOne({ _id: this.user }).exec().then((doc) => {
            reqObj.user_req_num = doc.req_num; //  used for creating and recreating
            reqObj.user_num = doc.user_num //   correct key derivation paths in keymaker
          })
          .catch(err => {
            errorMessage()
          })

        await Wallet.findOne({ _id: this.APP_WALLET }).exec().then((app_doc) => {
            reqObj.app_key = app_doc.SeedNode
          })
          .catch(err => {
            errorMessage()
          })

        reqObj.req_pub_key = keyMaker.makeMultiKey(reqObj, 'address')

        var request = new Request(reqObj)

        await request.save()
          .catch(err => {  // rolls back, removes request
            Request.findByIdAndRemove({ _id: reqObj._id }).exec().then(() => {
              errorMessage()
            })
          })

        function errorMessage(msg) {
          if (!msg) {
            msg = 'Probem with request'
          }
          this.error = true
          res.statusMessage = `${msg}, Request NOT made!`
          res.send()
          throw msg
        }
        if (!this.error) {
          let msg = 'Request made!'
          console.log(msg)
          res.statusMessage = msg
          res.send()
        }
      } catch (err) {
        console.log(err)
        return
      }

    }
    generateRequest()
  },

  get: function(req, res) {

    this.user = req.user
    this.friend = req.friend

    if (req.friend) {
      this.user = this.friend // allows api to also be used for listing both user and friends requests
    }
    // retreives request from collection and returns to view
    const retreiveRequests = async() => {
      try {
        await Request.find({ user_name: this.user })
          .exec().then((requests) => {
            var reqSend = [];
            var re;
            for (re = 0; re < requests.length; re++) {
              reqSend.push({
                item_name: requests[re].req_item_name,
                item_id: requests[re].req_item_id,
                req_value: requests[re].req_value,
                btc_total: requests[re].btc_total,
                btc_total_unconf: requests[re].btc_total_unconf,
                order_complete: requests[re].order_complete
              })
            }
            if (reqSend != 0) {
              res.send(reqSend);
            }
          })
      } catch (err) {
        console.log(err)
        return 'Problem finding requests'
      }
    }
    return retreiveRequests()
    //retreiveRequests()
  }
}
